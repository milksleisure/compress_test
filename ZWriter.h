#ifndef __ZWRITER_H_4OXYVAYM__
#define __ZWRITER_H_4OXYVAYM__

#include <zlib.h>
#include <string>

class ZWriter {
    public:
        ZWriter() {
            // Compression level 9, best compression
            // 0: no compression
            // 1: best speed
            // 9: best compression level
            gfp = gzopen("TEST.gz", "w9");
        }

        ~ZWriter() {
            gzclose(gfp);
        }

        void write(const std::string &s) {
            gzwrite(gfp, (void *) s.c_str(), s.size());
        }
    private:
        gzFile gfp;
};

#endif /* end of include guard: ZWRITER_H_4OXYVAYM */
