#ifndef __BZWRITER_H_L8K4QOQN__
#define __BZWRITER_H_L8K4QOQN__

#include <bzlib.h>
#include <cstdio>
#include <string>

class BZWriter {
    public:
        BZWriter() {
            fp = fopen("TEST.bz", "wb");

            int error = 0;
            const int BLOCK_MULTIPLIER = 7;
            bfp = BZ2_bzWriteOpen(&error, fp, BLOCK_MULTIPLIER, 0, 0);

            if(error != BZ_OK) {
                fprintf(stderr, "BZip pointer not open with error\n");
            }
        }

        ~BZWriter() {
            int error = 0;
            BZ2_bzWriteClose(&error, bfp, 0, NULL, NULL);
            if(error != BZ_OK) {
                fprintf(stderr, "BZip pointer close with error\n");
            }
        }

        void write(const std::string &s) {
            int error = 0;
            BZ2_bzWrite(&error, bfp, (void *) s.c_str(), s.size());
        }
    private:
        FILE* fp;
        BZFILE* bfp;
};

#endif /* end of include guard: BZWRITER_H_L8K4QOQN */

