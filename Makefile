.phony: clean

TARGET=test
all:
	g++ -c *.cc
	g++ -o ${TARGET} *.o -lbz2 -lz
clean:
	rm -f *.o *.bz *.gz ${TARGET}
