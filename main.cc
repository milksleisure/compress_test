#include <string>
#include "BZWriter.h"
#include "ZWriter.h"

int main(int argc, char *argv[])
{
    using std::string;
    BZWriter w;
    ZWriter z;

    w.write(string("This is a test\n"));
    w.write(string("Hello World\n"));

    z.write(string("This is a test\n"));
    z.write(string("Hello World\n"));
    return 0;
}
